/* Enable tooltip everywhere with data-toggle */
$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})

/* Animate text */
$(document).ready(function() {
            
    TEXT_NO = 0;

    function animateText() {
        let text = [
            "expense reports",
            "chasing receipts",
            "reimbursements"
        ];

        let timeout = 4000;
        if(TEXT_NO < 3) {
            let show = text[TEXT_NO];
            $('#textAnimate').fadeOut(1000, function() {
                $(this).text(show).fadeIn();
                TEXT_NO ++;
            });
        } else {
            TEXT_NO = 0;
            timeout = 0;
        }
        
        setTimeout(function() {
            animateText();
        }, timeout)

    }
    animateText();
})

/* Tabs alternate image on accordion click */

$(document).on("click","[data-activateimg]",e=>{
    $($(e.target).data("activateimg"))
        .addClass("active")
        .siblings().removeClass("active");
});